package com.rangers.BelajarSpringBootWeb.services;

import com.rangers.BelajarSpringBootWeb.model.Mahasiswa;

import java.util.List;

public interface MahasiswaService {

    List <Mahasiswa> listmahasiswa();
    Mahasiswa saveOrUpdate(Mahasiswa mahasiswa);
    Mahasiswa getIdMahasiswa(Integer id);
    void hapus(Integer id);
}
